//
// Created by Niek on 9-11-2018.
//

#include "Triangle.h"
#include <gtest/gtest.h>
#include <gmock/gmock.h>

using testing::Eq;

namespace {
    class Classdeclaration : public testing::Test{
    public:
        Niek::Triangle obj;
        Classdeclaration(){
            obj;
        }
    };
}

TEST_F(Classdeclaration, nameOfTheTest1){
    ASSERT_EQ("Equilateral",obj.triangle_type(2,2,2));
}

TEST_F(Classdeclaration, nameOfTheTest2){
    ASSERT_EQ("Isosceles",obj.triangle_type(5,5,2));
}

TEST_F(Classdeclaration, nameOfTheTest3){
    ASSERT_EQ("Scalene",obj.triangle_type(5,2,9));
}

TEST_F(Classdeclaration, nameOfTheTest4){
    ASSERT_EQ("Error",obj.triangle_type(2,5,-3));
}

TEST_F(Classdeclaration, nameOfTheTest5){
    ASSERT_EQ("Error",obj.triangle_type(2,0,3));
}