//
// Created by Niek on 9-11-2018.
//

#include "Triangle.h"

namespace Niek{
    Triangle::Triangle() {}

    std::string Triangle::triangle_type(int sideA, int sideB, int sideC) {
        std::string type;

        if(sideA <= 0 || sideB <= 0 || sideC <= 0){
            std::cout<<"Error: Unusable lenghts."<<std::endl;
            return "Error";
        }

        if(sideA==sideB && sideA==sideC && sideB==sideC){
            type = "Equilateral";
        }else if(sideA==sideB || sideA==sideC || sideB==sideC){
            type = "Isosceles";
        }else{
            type = "Scalene";
        }

        return type;
    }

}