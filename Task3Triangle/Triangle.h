//
// Created by Niek on 9-11-2018.
//

#ifndef TASK3TRIANGLE_TRIANGLE_H
#define TASK3TRIANGLE_TRIANGLE_H

#include <iostream>

namespace Niek {
    class Triangle {
    public:
        Triangle();

        std::string triangle_type(int sideA, int sideB, int sideC);
    };
}

#endif //TASK3TRIANGLE_TRIANGLE_H
