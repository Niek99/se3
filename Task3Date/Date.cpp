//
// Created by Niek on 9-11-2018.
//

#include "Date.h"
#include <string>

namespace Niek{

    Date::Date() {}

    void Date::setDate(int day, int month, int year) {
        if(check(day,month,year)) {
            this->day = day;
            this->month = month;
            this->year = year;
        } else std::cout<<"Error: Unrealistic date entered."<<std::endl;
    }

    bool Date::check(int day, int month, int year) {
        bool schrikkel = schrikkeljaar(year);
        bool possible = false;

        if(month <8 && month%2 == 1){
            if(day<32) possible = true;
        } else if(month<8 && month%2 ==0){
            if(day<31) possible = true;
        } else if(month>7 && month%2==0){
            if(day<32) possible = true;
        } else if(month>7 && month%2==1){
            if(day<31) possible = true;
        }

        if(month == 2){
            if(schrikkel==true && day>29) {
                possible = false;
            } else if(schrikkel == false && day>28){
                possible = false;
            }
        }

        if(day<1 || day>31 || month<1 || month>12) possible = false;

        return possible;
    }

    bool Date::schrikkeljaar(int year) {
        bool answer=false;
        if(year%4 == 0){
            if(year%100 == 0){
                if(year%400 ==0){
                    answer = true;
                } else answer = false;
            }else answer = true;
        }
        return answer;
    }

    void Date::increment(int daysExtra) {
        for(int i=0; i<daysExtra; i++){
            day++;
            if(check(day,month,year)==false){
                day=1;
                month++;
                if(check(day,month,year)==false){
                    month=1;
                    year++;
                }
            }
        }
    }

    std::string Date::toString() {
        std::string days,months,final;
        if(std::to_string(day).length()==1){
            days = "0"+std::to_string(day);
        } else days = std::to_string(day);

        if(std::to_string(month).length()==1){
            months = "0"+std::to_string(month);
        } else months = std::to_string(month);

        final = days+"-"+months+"-"+std::to_string(year);

        return final;
    }

}