//
// Created by Niek on 9-11-2018.
//

#ifndef TASK3DATE_DATE_H
#define TASK3DATE_DATE_H

#include <ctime>
#include <iostream>

namespace Niek {
    class Date {
    public:
        Date();

        void setDate(int day, int month, int year);

        void increment(int daysExtra);

        std::string toString();

        bool check(int day, int month, int year);

        bool schrikkeljaar(int year);

    private:
        int day;
        int month;
        int year;
    };

}
#endif //TASK3DATE_DATE_H
