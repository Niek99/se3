//
// Created by Niek on 9-11-2018.
//

#include "Date.h"
#include <gtest/gtest.h>
#include <gmock/gmock.h>

using testing::Eq;

namespace {
    class Classdeclaration : public testing::Test{
    public:
        Niek::Date obj;
        Classdeclaration(){
            obj;
        }
    };
}

TEST_F(Classdeclaration, nameOfTheTest1){
    obj.setDate(1,1,2000);                      //standaard test
    obj.increment(4);
    ASSERT_EQ(obj.toString(),"05-01-2000");
}

TEST_F(Classdeclaration, nameOfTheTest2){
    obj.setDate(29,1,2000);                      //test op maand-overgang
    obj.increment(4);
    ASSERT_EQ(obj.toString(),"02-02-2000");
}

TEST_F(Classdeclaration, nameOfTheTest3){
    obj.setDate(29,12,2000);                      //test op jaar-overgang
    obj.increment(4);
    ASSERT_EQ(obj.toString(),"02-01-2001");
}

TEST_F(Classdeclaration, nameOfTheTest4){
    obj.setDate(27,2,1600);                      //test op schrikkeljaar
    obj.increment(4);
    ASSERT_EQ(obj.toString(),"02-03-1600");
}

TEST_F(Classdeclaration, nameOfTheTest5){
    obj.setDate(27,2,1700);                      //test verschil in schrikkeljaar
    obj.increment(4);
    ASSERT_EQ(obj.toString(),"03-03-1700");
}
